package com.epam.model;

public class Ticket {
    private static int count = 1;
    private int id;
    private String from;
    private String to;

    public Ticket(String from, String to) {
        this.from = from;
        this.to = to;
        id = count;
        count++;
    }

    public String getTo() {
        return to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "ID = " + getId() + " \nFrom : " + getFrom() + "\nTo : " + getTo();
    }
}
