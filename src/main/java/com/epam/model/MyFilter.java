package com.epam.model;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.html.Option;
import java.io.IOException;
import java.util.Optional;

@WebFilter("/tickets/*")
public class MyFilter implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        Optional<String> ticketOptional = Optional.ofNullable(servletRequest.getParameter("to"));
        if (ticketOptional.filter(ticket -> ticket.equalsIgnoreCase("RU")).isPresent()) {
            ((HttpServletResponse) servletResponse).sendError(403, "NOT RUSSIA");
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy() {

    }
}
