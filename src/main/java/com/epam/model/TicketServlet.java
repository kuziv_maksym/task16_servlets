package com.epam.model;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/tickets/*")
public class TicketServlet extends HttpServlet {
    private static Map<Integer, Ticket> tickets = new HashMap<Integer, Ticket>();

    @Override
    public void init() throws ServletException {
        Ticket ticket1 = new Ticket( "UA", "UK");
        Ticket ticket2 = new Ticket( "UK", "UA");
        Ticket ticket3 = new Ticket( "UA", "TR");
        tickets.put(ticket1.getId(), ticket1);
        tickets.put(ticket2.getId(), ticket2);
        tickets.put(ticket3.getId(), ticket3);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        String title = "Passing parameters using GET method";
        PrintWriter writer = resp.getWriter();
        writer.println("<html>");
        writer.println("<head><title>" + title + "</title></head>");
        writer.println("<body>");
        for (Ticket ticket : tickets.values()) {
            writer.println("<p>" + ticket + "</p>");
        }
        writer.println("<form name=\"testForm\" method='POST'>");
        writer.println("<label><h1>Input<h1/></label><br/>");
        writer.println("From : <input type=\"text\" name=\"from\"><br/>");
        writer.println("To : <input type=\"text\" name=\"to\"><br/>");
        writer.println("<input type=\"submit\">");
        writer.println("</form>");

        writer.println("<form>");
        writer.println("<label><h1>Input<h1/></label><br/>");
        writer.println("ID : <input type=\"text\" name=\"ticket_id\"><br/>");
        writer.println("<input type='button' onclick='remove(this.form.ticket_id.value)' name='ok' value='Delete ticket'>");
        writer.println("</form>");
        writer.println("<script type='text/javascript'>\n"
        + " function remove(id) { fetch('tickets/' + id, {method: 'DELETE'});  } \n" +
                "</script>");
        writer.println("</body>");
        writer.println("</html>");
        writer.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String from = req.getParameter("from");
        String to = req.getParameter("to");
        Ticket ticket = new Ticket(from,to);
        tickets.put(ticket.getId(), ticket);
        doGet(req,resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getRequestURI();
        id = id.replace("/tickets/", "");
        tickets.remove(Integer.parseInt(id));
    }


}
